Fides 1.0.0 Release Notes
=========================

# Table of Contents
1. [Core](#Core)
    - Support for structured and unstructured grids
    - Support for ADIOS2 inline engine
    - Support for XGC
    - Support for wildcard fields
    - Add ability to automatically generate data model
    - Add field data feature
2. [Build](#Build)
    - Add RapidJSON as a third party library
3. [Other](#Other)
    - Change name from ADIS to Fides
    - Create user guide
    - Add schema validation tests


# Core

## Support for structured and unstructured grids ##

## Support for ADIOS2 inline engine ##

Inline engine can now be used with Fides.
Since inline engine requires the reader and writer to use the same `adios2::IO` object,
Fides can also be passed an IO object to be used for a given `DataSource` instead of creating its own.

## Support for XGC ##

XGC is now supported in Fides.
It is supported mostly using the Extruded mesh support in VTK-m.
There are two examples under examples/xgc (along with a readme explaining what's needed for the JSON file when reading XGC data).
xgc.cxx reads all data from files, while xgc-sst.cxx reads mesh from file but the 3d field data using SST engine.

## Support for wildcard fields ##

The fields to be read from an ADIOS2 variable do not need to be individually specified in a data model now.
See the [Fides User Guide](https://fides.readthedocs.io/en/latest/components/components.html#wildcard-fields) for details.

## Add ability to automatically generate data model ##

Fides now has support for automatically generating a data model based on attributes available in a BP file.
See the [Fides User Guide](https://fides.readthedocs.io/en/latest/components/components.html#data-model-generation) for details.

## Add field data feature ##

Fides can now manage data from ADIOS2 variables that do not have a points or cells association.
See the [Fides User Guide](https://fides.readthedocs.io/en/latest/components/components.html#field-data) for details.

# Build

## Add RapidJSON as a third party library ##

Since VTK uses a different JSON parser, we added RapidJSON as a third party library to Fides, so there isn't another JSON library added to VTK.

# Other

## Change name from ADIS to Fides ##

## Create user guide ##

User guide was created and is hosted at [Read The Docs](https://fides.readthedocs.io/en/latest/index.html).

## Add schema validation tests ##

A JSON schema has been added at `tests/fides-schema.json` and is used in validation testing (using ctest) for all of the data models used in the other tests.
Data models are not validated against the schema directly in Fides yet.
