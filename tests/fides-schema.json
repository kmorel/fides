{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "definitions": {
    "data_model": {
      "type": "object",
      "properties": {
        "data_sources": {
          "type": "array",
          "minItems": 1,
          "items": {
            "$ref": "#/definitions/data_sources_item"
          }
        },
        "fields": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/fields_item"
          }
        },
        "coordinate_system": {
          "$ref": "#/definitions/coordinate_system"
        },
        "cell_set": {
          "$ref": "#/definitions/cellset_alltype"
        },
        "step_information": {
          "data_source": {
            "type": "string"
          }
        },
        "number_of_planes": {
          "$ref": "#/definitions/value_scalar"
        }
      },
      "additionalProperties": false,
      "required": [
        "data_sources",
        "coordinate_system",
        "cell_set"
      ]
    },
    "data_sources_item": {
      "description": "A data source to read from",
      "type": "object",
      "properties": {
        "name": {
          "type": "string"
        },
        "filename_mode": {
          "description": "How the data source is organized (single file or folder)",
          "type": "string",
          "enum": [
            "input",
            "relative"
          ],
          "default": "input"
        },
        "filename": {
          "description": "Filename when using relative filename_mode",
          "type": "string"
        }
      },
      "additionalProperties": false,
      "required": [
        "name",
        "filename_mode"
      ]
    },
    "coordinate_system": {
      "description": "The named coordinate system used",
      "type": "object",
      "properties": {
        "array": {
          "$ref": "#/definitions/array_alltype"
        }
      },
      "additionalProperties": false,
      "required": ["array"]
    },
    "fields_item": {
      "description": "the fields",
      "type": "object",
      "properties": {
        "name": {
          "type": "string"
        },
        "association": {
          "type": "string",
          "enum": [
            "points",
            "cell_set",
            "field_data"
          ]
        },
        "variable_list_attribute_name": {
          "type": "string"
        },
        "variable_association_attribute_name": {
          "type": "string"
        },
        "variable_vector_attribute_name": {
          "type": "string"
        },
        "variable_sources_attribute_name": {
          "type": "string"
        },
        "variable_arrays_attribute_name": {
          "type": "string"
        },
        "array": {
          "$ref": "#/definitions/array_basic"
        }
      },
      "additionalProperties": false,
      "required": [
        "array"
      ]
    },
    "cellset_alltype": {
      "anyOf": [
        {
          "$ref": "#/definitions/cellset_structured"
        },
        {
          "$ref": "#/definitions/cellset_single"
        },
        {
          "$ref": "#/definitions/cellset_explicit"
        },
        {
          "$ref": "#/definitions/cellset_xgc"
        }
      ]
    },
    "cellset_structured": {
      "description": "The cell set in vtkm",
      "type": "object",
      "properties": {
        "cell_set_type": {
          "type": "string",
          "const": "structured"
        },
        "dimensions": {
          "$ref": "#/definitions/value_alltype"
        }
      },
      "additionalProperties": false,
      "required": [
        "cell_set_type",
        "dimensions"
      ]
    },
    "cellset_single": {
      "description": "The cell set in vtkm",
      "type": "object",
      "properties": {
        "cell_set_type": {
          "type": "string",
          "const": "single_type"
        },
        "cell_type": {
          "type": "string",
          "enum": [
            "vertex",
            "line",
            "triangle",
            "quad",
            "tetrahedron",
            "hexahedron",
            "wedge",
            "pyramid"
          ]
        },
        "variable": {
          "type": "string"
        },
        "static": {
          "type": "boolean"
        }
      },
      "additionalProperites": false,
      "required": [
        "cell_set_type",
        "cell_type",
        "variable"
      ]
    },
    "cellset_explicit": {
      "description": "The cell set in vtkm",
      "type": "object",
      "properties": {
        "cell_set_type": {
          "type": "string",
          "const": "explicit"
        },
        "cell_types": {
          "$ref": "#/definitions/array_alltype"
        },
        "number_of_vertices": {
          "$ref": "#/definitions/array_alltype"
        },
        "connectivity": {
          "$ref": "#/definitions/array_alltype"
        },
        "static": {
          "type": "boolean"
        }
      },
      "additionalProperties": false,
      "required": [
        "cell_set_type",
        "cell_types",
        "number_of_vertices",
        "connectivity"
      ]
    },
    "cellset_xgc": {
      "description": "The cell set in vtkm",
      "type": "object",
      "properties": {
        "cell_set_type": {
          "type": "string",
          "const": "xgc"
        },
        "periodic": {
          "type": "boolean"
        },
        "cells": {
          "$ref": "#/definitions/array_alltype"
        },
        "plane_connectivity": {
          "$ref": "#/definitions/array_alltype"
        },
        "static": {
          "type": "boolean"
        }
      },
      "additionalProperties": false,
      "required": [
        "cell_set_type",
        "cells",
        "plane_connectivity"
      ]
    },
    "array_alltype": {
      "anyOf": [
        {
          "$ref": "#/definitions/array_basic"
        },
        {
          "$ref": "#/definitions/array_uniform_point"
        },
        {
          "$ref": "#/definitions/array_cartesian_product"
        },
        {
          "$ref": "#/definitions/array_xgc_coordinates"
        },
        {
          "$ref": "#/definitions/array_xgc_field"
        }
      ]
    },
    "array_basic": {
      "type": "object",
      "properties": {
        "array_type": {
          "type": "string",
          "const": "basic"
        },
        "data_source": {
          "type": "string"
        },
        "variable": {
          "type": "string"
        },
        "static": {
          "type": "boolean"
        },
        "is_vector": {
          "type": "string",
          "enum": [
            "true",
            "false",
            "auto"
          ]
        }
      },
      "additionalProperties": false,
      "required": [
        "data_source",
        "variable",
        "array_type"
      ]
    },
    "array_uniform_point": {
      "type": "object",
      "properties": {
        "array_type": {
          "type": "string",
          "const": "uniform_point_coordinates"
        },
        "dimensions": {
          "$ref": "#/definitions/value_alltype"
        },
        "origin": {
          "$ref": "#/definitions/value_alltype"
        },
        "spacing": {
          "$ref": "#/definitions/value_alltype"
        }
      },
      "additionalProperties": false,
      "required": [
        "array_type",
        "dimensions"
      ]
    },
    "array_cartesian_product": {
      "type": "object",
      "properties": {
        "array_type": {
          "type": "string",
          "const": "cartesian_product"
        },
        "x_array": {
          "$ref": "#/definitions/array_basic"
        },
        "y_array": {
          "$ref": "#/definitions/array_basic"
        },
        "z_array": {
          "$ref": "#/definitions/array_basic"
        }
      },
      "additionalProperties": false,
      "required": [
        "array_type",
        "x_array",
        "y_array",
        "z_array"
      ]
    },
    "array_xgc_coordinates": {
      "type": "object",
      "properties": {
        "array_type": {
          "type": "string",
          "enum": [
            "xgc_coordinates"
          ]
        },
        "data_source": {
          "type": "string"
        },
        "variable": {
          "type": "string"
        },
        "static": {
          "type": "boolean"
        },
        "is_cylindrical": {
          "type": "boolean"
        }
      },
      "required": [
        "data_source",
        "variable"
      ]
    },
    "array_xgc_field": {
      "type": "object",
      "properties": {
        "array_type": {
          "type": "string",
          "enum": [
            "xgc_field_plane"
          ]
        },
        "data_source": {
          "type": "string"
        },
        "variable": {
          "type": "string"
        },
        "static": {
          "type": "boolean"
        }
      },
      "required": [
        "data_source",
        "variable"
      ]
    },
    "value_alltype": {
      "anyOf": [
        {
          "$ref": "#/definitions/value_var_dimensions"
        },
        {
          "$ref": "#/definitions/value_array"
        },
        {
          "$ref": "#/definitions/value_scalar"
        }
      ]
    },
    "value_var_dimensions": {
      "description": "A value loaded from json file or data file",
      "type": "object",
      "properties": {
        "source": {
          "type": "string",
          "const": "variable_dimensions"
        },
        "data_source": {
          "type": "string"
        },
        "variable": {
          "type": "string"
        },
        "static": {
          "type": "boolean"
        }
      },
      "additionalProperties": false,
      "required": [
        "source",
        "data_source",
        "variable"
      ]
    },
    "value_scalar": {
      "type": "object",
      "properties": {
        "source": {
          "type": "string",
          "const": "scalar"
        },
        "data_source": {
          "type": "string"
        },
        "variable": {
          "type": "string"
        }
      },
      "additionalProperties": false,
      "required": [
        "source",
        "data_source",
        "variable"
      ]
    },
    "value_array": {
      "description": "A value loaded from json file or data file",
      "type": "object",
      "properties": {
        "source": {
          "type": "string",
          "const": "array"
        },
        "values": {
          "type": "array",
          "items": {
            "type": "number"
          }
        }
      },
      "additionalProperties": false,
      "required": [
        "source",
        "values"
      ]
    }
  },

  "type": "object",
  "properties": {},
  "additionalProperties": {
    "$ref": "#/definitions/data_model"
  }
}
