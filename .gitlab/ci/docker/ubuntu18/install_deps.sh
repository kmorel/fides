#!/bin/sh

# System cmake/ninja are required for building dependencies

# Install build requirements
apt-get update -y
apt-get install build-essential sudo curl \
                python3-dev python3-numpy python3-pip \
                libbz2-dev libpng-dev rapidjson-dev \
                cmake ninja-build \
                git git-lfs vim \
                libssl-dev \
                clang-8 \
                -y
git lfs install
