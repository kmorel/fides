if (VTKm_VERSION VERSION_EQUAL "1.9")
  set(VTKM_LIBS vtkm_filter)
  if (VTKm_ENABLE_RENDERING)
    set(RENDERING_DEFS USE_VTKM_RENDERING)
    set(RENDERING_LIBS vtkm_rendering)
  endif()
elseif(VTKm_VERSION VERSION_GREATER_EQUAL "2.0")
  set(VTKM_LIBS vtkm::filter)
  if (VTKm_ENABLE_RENDERING)
    set(RENDERING_DEFS USE_VTKM_RENDERING)
    set(RENDERING_LIBS vtkm::rendering)
  endif()
endif()
add_executable(xgc xgc.cxx)
vtkm_add_target_information(xgc
                            DROP_UNUSED_SYMBOLS
                            MODIFY_CUDA_FLAGS
                            DEVICE_SOURCES xgc.cxx)
target_compile_definitions(xgc PRIVATE ${MPI_DEFS} ${RENDERING_DEFS})
target_link_libraries(xgc fides adios2::adios2 ${VTKM_LIBS} ${RENDERING_LIBS} ${MPI_LIBS})

add_executable(xgc-sst xgc-sst.cxx)
vtkm_add_target_information(xgc-sst
                            DROP_UNUSED_SYMBOLS
                            MODIFY_CUDA_FLAGS
                            DEVICE_SOURCES xgc-sst.cxx)
target_compile_definitions(xgc-sst PRIVATE ${MPI_DEFS} ${RENDERING_DEFS})
target_link_libraries(xgc-sst fides adios2::adios2 ${VTKM_LIBS} ${RENDERING_LIBS} ${MPI_LIBS})
