#------------------------------------------------------------------------------#
# Distributed under the OSI-approved Apache License, Version 2.0.  See
# accompanying file Copyright.txt for details.
#------------------------------------------------------------------------------#
#
# FindRapidJSON
# -----------
#
# Try to find the RapidJSON library
#
# This module defines the following variables:
#
#   RapidJSON_FOUND        - System has RapidJSON
#   RapidJSON_INCLUDE_DIRS - The RapidJSON include directories used by
#                            dependant targets
#
# and the following imported targets:
#   RapidJSON::RapidJSON - The RapidJSON library target
#
# You can also set the following variable to help guide the search:
#   RapidJSON_INCLUDE_DIR - The include directory containing RapidJSON headers

if(NOT RapidJSON_FOUND)

  find_path(RapidJSON_INCLUDE_DIR rapidjson/rapidjson.h)

  # Parse version information from the header
  if(RapidJSON_INCLUDE_DIR)
    file(STRINGS ${RapidJSON_INCLUDE_DIR}/rapidjson/rapidjson.h
      _RapidJSON_VERSION_MATCHES
      REGEX "^#define +RAPIDJSON_([^_]*)_VERSION +(.*)"
    )
    foreach(ver_string IN LISTS _RapidJSON_VERSION_MATCHES)
      if(ver_string MATCHES "^#define +RAPIDJSON_([^_]*)_VERSION +(.*)")
        set(RapidJSON_VERSION_${CMAKE_MATCH_1} ${CMAKE_MATCH_2})
      endif()
    endforeach()
    set(RapidJSON_VERSION "${RapidJSON_VERSION_MAJOR}.${RapidJSON_VERSION_MINOR}.${RapidJSON_VERSION_PATCH}")
  endif()

  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(RapidJSON
    FOUND_VAR RapidJSON_FOUND
    REQUIRED_VARS RapidJSON_INCLUDE_DIR
    VERSION_VAR RapidJSON_VERSION
  )
  if(RapidJSON_FOUND)
    set(RapidJSON_INCLUDE_DIRS ${RapidJSON_INCLUDE_DIR})
    if(RapidJSON_FOUND AND NOT TARGET RapidJSON::RapidJSON)
      add_library(RapidJSON::RapidJSON INTERFACE IMPORTED)
      set_target_properties(RapidJSON::RapidJSON PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${RapidJSON_INCLUDE_DIRS}"
      )
    endif()
  endif()
endif()
